using System;

namespace BlabberApi.Utils
{
    public static class GuidUtils
    {
        public static string GetNewGuid()
        {
            return Guid.NewGuid().ToString();
        }

        public static string GetNewDoubleGuid()
        {
            return Guid.NewGuid().ToString()+Guid.NewGuid().ToString();
        }

        public static string GetNewTripleGuid()
        {
            return Guid.NewGuid().ToString()+Guid.NewGuid().ToString()+Guid.NewGuid().ToString();
        }
    }
}