namespace BlabberApi.Constants
{
    public static class TokenConstants
    {
        public static readonly string Secret = "JKxeHMDRH4gqwVhPxJnXx3cFi65aVb";

        public static class ClaimTypes
        {
            public static readonly string Uid = "uid";

            public static readonly string Cls = "cls";
        }
    }
}