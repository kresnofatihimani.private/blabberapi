using System.Collections.Generic;

namespace BlabberApi.Constants
{
    public static class AuthValidationParams
    {
        public static class Types
        {
            public const string UidOnly = "UidOnly";

            public const string MasterClassOnly = "MasterClassOnly";
        }

        public static Dictionary<string, string> GetByType(string type)
        {
            switch(type)
            {
                case Types.MasterClassOnly:
                    return new Dictionary<string, string>()
                    {
                        {TokenConstants.ClaimTypes.Cls, UserClasses.Master}
                    };
                default:
                    return null;
            }
        }
    }
}