namespace BlabberApi.Constants
{
    public static class HeaderTypes
    {
        public static readonly string AccessToken = "blb-access-token";
    }
}