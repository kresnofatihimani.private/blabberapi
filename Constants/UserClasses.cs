namespace BlabberApi.Constants
{
    public static class UserClasses
    {
        public static readonly string Suspended = "Suspended";

        public static readonly string Basic = "Basic";

        public static readonly string Master = "Master";
    }
}