using System;
using System.ComponentModel.DataAnnotations;

namespace BlabberApi.Models.DTOs.UserDTOs
{
    public class NewUserReq
    {
        [Required]
        [MaxLength(30)]
        public string UserId {get; set;}

        [Required]
        [MaxLength(30)]
        public string FullName {get; set;}

        [Required]
        public string Password {get; set;}

        [Required]
        public DateTime DateOfBirth {get; set;}
    }
}