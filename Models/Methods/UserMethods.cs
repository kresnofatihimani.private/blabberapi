using System;
using BlabberApi.Constants;
using BlabberApi.Models.DTOs.UserDTOs;
using BlabberApi.Utils;

namespace BlabberApi.Models.Methods
{
    public static class UserMethods
    {
        public static User toNewUser(this NewUserReq newUserReq)
        {
            return new User()
            {
                UserId          =   newUserReq.UserId,
                FullName        =   newUserReq.FullName,
                Bio             =   null,
                HashedPassword  =   newUserReq.Password.toHashedPassword(),
                DateOfBirth     =   newUserReq.DateOfBirth,
                CreatedAt       =   DateTime.UtcNow,
                PhotoUrl        =   null,
                Class           =   UserClasses.Basic
            };
        }

        public static void updateUser(this User user, UpdateUserReq updateUserReq)
        {
            user.FullName   = updateUserReq.FullName;
            user.Bio        = updateUserReq.Bio;
            user.PhotoUrl   = updateUserReq.PhotoUrl;
        }
    }
}