using System;
using BlabberApi.Constants;
using BlabberApi.Utils;

namespace BlabberApi.Models.Methods
{
    public static class RelationMethods
    {
        public static Relation toNewConnectedRelation(this string toUserId, string userId)
        {
            return new Relation()
            {
                RelationId      =   GuidUtils.GetNewDoubleGuid(),
                UserId          =   userId,
                ToUserId        =   toUserId,
                Relationship    =   RelationshipTypes.Connected,
                CreatedAt       =   DateTime.UtcNow
            };
        }

        public static Relation toNewPendingRelation(this string toUserId, string userId)
        {
            return new Relation()
            {
                RelationId      =   GuidUtils.GetNewDoubleGuid(),
                UserId          =   userId,
                ToUserId        =   toUserId,
                Relationship    =   RelationshipTypes.Pending,
                CreatedAt       =   DateTime.UtcNow
            };
        }
    }
}