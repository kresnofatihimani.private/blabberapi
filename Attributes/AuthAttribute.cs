using System.Collections.Generic;
using BlabberApi.Constants;
using BlabberApi.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace BlabberApi.Attributes
{
    public class AuthAttribute  :   ActionFilterAttribute
    {
        private string _validationParamsType;

        public AuthAttribute()
        {
            //
        }

        public AuthAttribute(string validationParamsType)
        {
            _validationParamsType = validationParamsType;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            string accessToken = context.HttpContext.Request.Headers[HeaderTypes.AccessToken];
            bool isValid = false;
            if(_validationParamsType.isNull())
            {
                isValid = accessToken.ValidateClaims();
            }
            else
            {
                isValid = accessToken.ValidateClaims(_validationParamsType);
            }
            
            if (!isValid)
            {
                context.Result = new BadRequestObjectResult("not authed");
            }
            else
            {
                string newToken = accessToken.RefreshToken();
                context.HttpContext.Response.Headers.Add(HeaderTypes.AccessToken, newToken);
                base.OnActionExecuting(context);
            }
        }
    }
}