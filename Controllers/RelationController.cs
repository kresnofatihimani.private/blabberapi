using System.Threading.Tasks;
using BlabberApi.Attributes;
using BlabberApi.Constants;
using BlabberApi.Controllers.Functions;
using BlabberApi.Data;
using BlabberApi.Models;
using BlabberApi.Utils;
using Microsoft.AspNetCore.Mvc;

namespace BlabberApi.Controllers
{
    [Route("api/[controller]")]
    public class RelationController :   ControllerBase
    {
        private AppDbContext _appDbContext;

        private RelationFunctions _relationFunctions;

        public RelationController(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
            _relationFunctions = new RelationFunctions(appDbContext);
        }

        [Auth]
        [HttpPost("connect/{toUserId}")]
        public async Task<ActionResult<Relation>> ConnectToUser(string toUserId)
        {
            var token = Request.Headers[HeaderTypes.AccessToken];
            string userId = (string) TokenUtils.ExtractClaim(token, TokenConstants.ClaimTypes.Uid);
            var res = await _relationFunctions.FunctionConnectToUser(toUserId, userId);
            if(res.isNull())
            {
                return BadRequest("blocked");
            }
            return Ok(res);
        }
    }
}