using System.Threading.Tasks;
using BlabberApi.Constants;
using BlabberApi.Data;
using BlabberApi.Models;
using BlabberApi.Models.Methods;
using BlabberApi.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BlabberApi.Controllers.Functions
{
    [NonController]
    public class RelationFunctions  :   ControllerBase
    {
        public AppDbContext _appDbContext;

        public RelationFunctions(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public async Task<Relation> FunctionConnectToUser(string toUserId, string userId)
        {
            if(!toUserId.isNull() && !userId.isNull())
            {
                Relation peerRelation = await GetPeerRelation(toUserId, userId);

                if(!peerRelation.isNull())
                {
                    bool blockedByPeer = (peerRelation.Relationship == RelationshipTypes.Blocked);
                    if(blockedByPeer)
                    {
                        return null;
                    }

                    bool peerRelationIsPending = (peerRelation.Relationship == RelationshipTypes.Pending);
                    if(peerRelationIsPending)
                    {
                        // change peerRelation relationship to connected
                        peerRelation.Relationship = RelationshipTypes.Connected;

                        // create new relation to peer as connected relation
                        Relation newConnectedRelation = toUserId.toNewConnectedRelation(userId);
                        await _appDbContext.Relations.AddAsync(newConnectedRelation);

                        // save changes
                        await _appDbContext.SaveChangesAsync();
                        return newConnectedRelation;
                    }
                }

                // if hasn't been any contact any all between two users
                Relation newPendingRelation = toUserId.toNewPendingRelation(userId);
                await _appDbContext.Relations.AddAsync(newPendingRelation);
                await _appDbContext.SaveChangesAsync();
                return newPendingRelation;

            }
            return null;
        }

        public async Task<Relation> GetPeerRelation(string toUserId, string userId)
        {
            Relation peerRelation = await _appDbContext
                .Relations
                .SingleOrDefaultAsync(
                    t => (t.UserId == toUserId && t.ToUserId == userId)
                );
            return peerRelation;
        }
    }
}