using BlabberApi.Models;
using Microsoft.EntityFrameworkCore;

namespace BlabberApi.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) :   base(options)
        {
            //
        }

        public DbSet<User> Users {get; set;}

        public DbSet<Relation> Relations {get; set;}
    }
}